-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local l = require'lpeg'
local P, S, R, Cc = l.P, l.S, l.R, l.Cc

local lispwords = {
	-- If none of the string keys matched, this pattern is attempted
	[0] = ('def' * (
		'ine-' * P(1)^1 +
		(P'class' + 'constant' + 'package' + 'parameter' + 'struct' + 'var') * -P(1)) +
		'with-' * P(1)^1) * Cc(1),

	['defmethod'] = 100,  -- TODO

	['block'] = 1,
	['case'] = 1,
	['collect'] = 1,
	['do-external-symbols'] = 1,
	['dolist'] = 1,
	['dotimes'] = 1,
	['ecase'] = 1,
	['etypecase'] = 1,
	['eval-when'] = 1,
	['flet'] = 1,
	['handler-bind'] = 1,
	['handler-case'] = 1,
	['ignore-errors'] = 1,
	[':implementation'] = 1,
	['labels'] = 1,
	['lambda'] = 1,
	['let'] = 1,
	['let*'] = 1,
	['loop'] = 1,
	['macrolet'] = 1,
	[':method'] = 1,
	['print-unreadable-object'] = 1,
	['prog1'] = 1,
	['progn'] = 1,
	['tagbody'] = 1,
	['typecase'] = 1,
	['unless'] = 1,
	['unwind-protect'] = 1,
	['when'] = 1,

	['defgeneric'] = 2,
	['defmacro'] = 2,
	['deftype'] = 2,
	['defun'] = 2,
	['destructuring-bind'] = 2,
	['do'] = 2,
	['do*'] = 2,
	['multiple-value-bind'] = 2,
	['with-slots'] = 2,

	['defsetf'] = 3,
}

-- TODO: add more prefixes
local macro_prefix =
	P'#' * (
		S"':.+-BbOoXxCcSsPp" +
		R'09'^0 * S'*=#AaRr'
		)^-1 +
	',' * S'@.'^-1 +
	S"`'"

local delimiters = {
	['('] = ')',
	['"'] = '"',
	['|'] = '|',
	[';']  = '\n',
	['#|'] = '|#',
}

return {
	prefix = macro_prefix,
	opposite = delimiters,
	lispwords = lispwords,
}
