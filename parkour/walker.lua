-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local M = {}

local function finishof(node) return node.finish end
local function startof(node) return node.start end

function M.new(parser, eol_at, bol_at)

	local function in_quasilist(t, range)
		return t and t.d and not t.is_list and
			range.start >= t.start + (t.p and #t.p or 0) + #t.d and range.finish <= t.finish + 1 - #parser.opposite[t.d]
	end

	local function sexp_at(range, true_sexp)
		local node, parent, nth = parser.tree.sexp_at(range)
		if true_sexp or not in_quasilist(node, range) then
			return node, parent, nth
		else
			return node.word_at(range), node
		end
	end

	local function escaped_at(range)
		local node = parser.escaped.around(range)
		-- TODO: distinguish between doublequoted strings, chars, line comments, and block comments
		-- and use approprite offset comparisons for each
		return node and range.start > node.start and range.finish <= node.finish and node
	end

	local function _find_before_innermost(t, range, key, pred)
		if t.is_root or (t.d and t.start < range.start) then
			if t.d and not t.is_list then
				local keypos = key(t)
				local start = keypos == startof(t)
				local finish = keypos == finishof(t)
				local newpos
				if finish then
					newpos = t.finish_before(range.finish)
				elseif start then
					newpos = t.start_before(range.start)
				end
				if newpos then
					local word = t.word_at({start = newpos, finish = newpos})
					if word and (not pred or pred(word)) then
						return word
					end
				end
				return
			end
			local _, nearest_n = t.before(range.start, startof)
			if nearest_n then
				for n = nearest_n, 1, -1 do
					local child = t[n]
					if child.d and not child.is_empty then
						local ret = _find_before_innermost(child, range, key, pred)
						if ret then
							return ret
						end
					elseif key(child) < key(range) and (not pred or pred(child, n, #t)) then
						return child, n, #t
					end
				end
			end
			if not t.is_root and (not pred or pred(t)) then
				return t
			end
		end
	end

	local function _find_after_innermost(t, range, key, pred)
		if t.is_root or (t.d and t.finish >= range.finish) then
			if t.d and not t.is_list then
				local keypos = key(t)
				local start = keypos == startof(t)
				local finish = keypos == finishof(t)
				local newpos
				if finish then
					-- TODO: does this need #t.p as well?
					-- why don't I handle #t.d and #t.p inside the methods instead?
					newpos = t.finish_after(math.max(t.start + #t.d, range.finish))
				elseif start then
					newpos = t.start_after(range.start)
				end
				if newpos then
					local word = t.word_at({start = newpos, finish = newpos})
					if word and (not pred or pred(word)) then
						return word
					end
				end
				return
			end
			local _, nearest_n = t.after(range.finish, finishof)
			if nearest_n then
				for n = nearest_n, #t do
					local child = t[n]
					if child.d and not child.is_empty then
						local ret = _find_after_innermost(child, range, key, pred)
						if ret then
							return ret
						end
					elseif key(child) >= key(range) and (not pred or pred(child, n, #t)) then
						return child, n, #t
					end
				end
			end
			if not t.is_root and (not pred or pred(t)) then
				return t
			end
		end
	end

	local function find_before_innermost(...)
		return _find_before_innermost(parser.tree, ...)
	end

	local function find_after_innermost(...)
		return _find_after_innermost(parser.tree, ...)
	end

	return {

		start_before = function(range, skip)
			local escaped = escaped_at(range)
			if escaped then
				local newpos = escaped.start_before(range.start)
				if newpos then
					local word = escaped.word_at({start = newpos, finish = newpos})
					return word and word.start
				else
					local _, parent, n = sexp_at(range, true)
					local cur, prev = parent[n], parent[n - 1]
					if prev and cur.is_line_comment and prev.is_line_comment then
						return prev.start_before(range.start)
					end
				end
			else
				local _, parent = parser.tree.sexp_at(range)
				local node = parent.before(range.start, startof, skip)
				return node and startof(node)
			end
		end,

		start_after = function(range, skip)
			local escaped = escaped_at(range)
			if escaped then
				local newpos = escaped.start_after(range.start)
				if newpos then
					local word = escaped.word_at({start = newpos, finish = newpos})
					return word and word.start
				else
					local _, parent, n = sexp_at(range, true)
					local cur, nxt = parent[n], parent[n + 1]
					if nxt and cur.is_line_comment and nxt.is_line_comment then
						return nxt.start_after(range.start)
					end
				end
			else
				local _, parent = parser.tree.sexp_at(range)
				local node = parent.after(range.finish, startof, skip)
				return node and startof(node)
			end
		end,

		finish_before = function(range, skip)
			local escaped = escaped_at(range)
			if escaped then
				local newpos = escaped.finish_before(range.finish)
				if newpos then
					local word = escaped.word_at({start = newpos, finish = newpos})
					return word and word.finish + 1
				else
					local _, parent, n = sexp_at(range, true)
					local cur, prev = parent[n], parent[n - 1]
					if prev and cur.is_line_comment and prev.is_line_comment then
						return prev.finish_before(range.start)
					end
				end
			else
				local _, parent = parser.tree.sexp_at(range)
				local node = parent.before(range.start, finishof, skip)
				return node and finishof(node) + 1
			end
		end,

		finish_after = function(range, skip)
			local escaped = escaped_at(range)
			if escaped then
				local newpos = escaped.finish_after(math.max(escaped.start + #escaped.d, range.finish))
				if newpos then
					local word = escaped.word_at({start = newpos, finish = newpos})
					return word and word.finish + 1
				else
					local _, parent, n = sexp_at(range, true)
					local cur, nxt = parent[n], parent[n + 1]
					if nxt and cur.is_line_comment and nxt.is_line_comment then
						return nxt.finish_after(range.start)
					end
				end
			else
				local _, parent = parser.tree.sexp_at(range)
				local node = parent.after(range.finish, finishof, skip)
				return node and finishof(node) + 1
			end
		end,

		start_down_after = function(range, skip)
			local node, parent = parser.tree.sexp_at(range)
			if in_quasilist(node, range) then return end
			if node and node.p then
				-- XXX: if we are past a prefix start, this will prevent skipping over its list
				range.start = node.start
			end
			local next_list = parent.find_after(range, function(t) return t.is_list end)
			if next_list then
				local first = next_list.after(range.finish, startof, skip)
				if first then
					return first.start
				else
					return next_list.start + (next_list.p and #next_list.p or 0) + 1
				end
			end
		end,

		start_up_before = function(range)
			local _, parent = sexp_at(range)
			return parent.d and parent.start
		end,

		finish_down_before = function(range, skip)
			local node, parent = parser.tree.sexp_at(range)
			if in_quasilist(node, range) then return end
			local prev_list = parent.find_before(range, function(t) return t.is_list end)
			if prev_list then
				local last = prev_list.before(range.start, finishof, skip)
				if last then
					return (last.finish or last.p and last.start + #last.p) + 1, prev_list
				else
					return prev_list.finish, prev_list
				end
			end
		end,

		finish_up_after = function(range)
			local _, parent = sexp_at(range)
			return parent.d and parent.finish + 1
		end,

		indented_before = function(range)
			return find_before_innermost(range, startof, function(t) return t.indent end)
		end,

		start_float_before = function(range, up_empty_list)
			if up_empty_list then
				local _, parent = sexp_at(range)
				if parent.is_empty then
					return parent.start, parent
				end
			end
			local node = find_before_innermost(range, startof, function(t, n, _)
				return not t.d or t.is_empty and n == 1 end)
			return node and node.start, node
		end,

		start_float_after = function(range)
			local r = {start = range.finish, finish = range.finish}
			local node = find_after_innermost(r, startof, function(t, n, max_n)
				-- XXX: max_n may not be correct at top level, due to the incremental parsing:
				return not t.d or t.is_empty and n == max_n end)
			return node and node.start, node
		end,

		finish_float_before = function(range)
			local r = {start = range.start, finish = range.start}
			local node = find_before_innermost(r, finishof, function(t, n, _)
				return not t.d or t.is_empty and n == 1 end)
			return node and node.finish + 1, node
		end,

		finish_float_after = function(range, up_empty_list)
			if up_empty_list then
				local _, parent = sexp_at(range)
				if parent.is_empty then
					return parent.finish + 1, parent
				end
			end
			local node = find_after_innermost(range, finishof, function(t, n, max_n)
				-- XXX: max_n may not be correct at top level, due to the incremental parsing:
				return not t.d or t.is_empty and n == max_n end)
			return node and node.finish + 1, node
		end,

		prev_start_wrapped = function(range)
			local node, parent = parser.tree.sexp_at(range)
			if in_quasilist(node, range) then parent = node end
			local pstart = parent.start and (parent.start + (parent.p and #parent.p or 0) + #parent.d)
			local bol = bol_at(range.start)
			local prev_wrapped = (parent.is_list or parent.is_root) and parent.find_before(range,
				function(t) return t.start < bol and t.finish > bol end,
				function(t) return t.finish < bol
			end)
			local prev_start = prev_wrapped and prev_wrapped.start or bol
			pstart = pstart or prev_start
			return math.max(pstart, prev_start)
		end,

		next_finish_wrapped = function(range)
			local node, parent = parser.tree.sexp_at(range)
			if in_quasilist(node, range) then parent = node end
			local eol = eol_at(range.start)
			local pfinish = parent.finish and parent.finish < eol and parent.finish + 1 - #parser.opposite[parent.d]
			local next_wrapped = not pfinish and (parent.is_list or parent.is_root) and parent.find_after(range,
				function(t) return t.start < eol and t.finish > eol end,
				function(t) return t.start > eol
			end)
			local next_finish = pfinish or next_wrapped and next_wrapped.finish + 1
			-- XXX: second return value, to be used for safe line-commenting:
			local next_line_break = pfinish or next_wrapped and next_wrapped.start
			return next_finish or eol, next_line_break
		end,

		anylist_start = function(range)
			local _, parent = sexp_at(range)
			return parent.start and (parent.start + (parent.p and #parent.p or 0) + #parent.d)
		end,

		anylist_finish = function(range)
			local _, parent = sexp_at(range)
			return parent.finish and parent.finish + 1 - #parser.opposite[parent.d]
		end,

		wider_than = function(range)
			local node, parent = parser.tree.sexp_at(range)
			if not node and parent.is_root then return end
			local pstart, pfinish
			if in_quasilist(node, range) then
				parent = node
				node = node.word_at(range)
			end
			if not parent.is_root then
				pstart, pfinish = parent.start + (parent.p and #parent.p or 0) + #parent.d,
					parent.finish + 1 - #parser.opposite[parent.d]
			end
			local same_as_inner = range.start == pstart and range.finish == pfinish
			local same_as_node = node and range.start == node.start and range.finish - 1 == node.finish
			if node and not same_as_node then
				return node.start, node.finish + 1
			elseif not same_as_inner then
				return pstart, pfinish
			end
			return parent.start, parent.finish + 1
		end,

		-- opening: nil - bracketed list, false - anylist (including a quasilist)
		list_at = function(range, opening)
			local lcd = parser.opposite["\n"]
			if opening and opening:match('[%' .. lcd .. '%"]') then
				local escaped = escaped_at(range)
				return escaped and escaped.d:find("^"..opening) and escaped
			else
				local _, nodes = parser.tree.sexp_path(range)
				local parent
				local up = #nodes - (opening == false and in_quasilist(nodes[#nodes], range) and 0 or 1)
				repeat
					parent = nodes[up]
					up = up - 1
				until not parent or (opening == false or not opening or parent.d == opening)
				return parent
			end
		end,

		repl_line_at = function(range)
			local last, nl = parser.tree.find_after(range, function(t) return t.indent end)
			local first, nf
			local node = parser.tree.around(range)
			if last and node and not node.is_comment and last.start == node.start then
				local i = nl
				repeat
					i = i + 1
				until not parser.tree[i] or parser.tree[i].indent
				nf = nl
				nl = i - 1
			else
				local i = nl or #parser.tree + 1
				repeat
					i = i - 1
				until i == 0 or parser.tree[i].indent
				nf = parser.tree[i] and not parser.tree[i].is_comment and i
				nl = nl and nl - 1 or #parser.tree
			end
			first, last = nf and parser.tree[nf], parser.tree[nl]
			if first and last then
				return {start = first.start, finish = last.finish}
			end
		end,

		sexp_at = sexp_at,
		escaped_at = escaped_at,
		paragraph_at = parser.tree.around,
		goto_path = parser.tree.goto_path,
		sexp_path = parser.tree.sexp_path,
		find_after = parser.tree.find_after,
		find_before = parser.tree.find_before,
	}
end

return M
