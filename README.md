howl-parkour is a structured editing plugin for the [Howl editor](https://github.com/howl-editor/howl),
based on the [parkour library](https://repo.or.cz/lisp-parkour.git).

# Configuration

In `~/.howl/init.lua` or `~/.howl/init.moon`:

    -- optional (example values):
    howl.config.auto_square_brackets = true
    howl.config.lispwords.scheme.lambda = 0
    howl.config.repl_fifo = os.getenv('HOME')..'/.repl_fifo'
    howl.config.emacs = true
    howl.config.compat = true

These options can also be set interactively via the `:set` command (except `lispwords`).

## `auto_square_brackets`

Rewrites any delimiter to a square bracket at certain locations.  
Has effect only on Scheme; for Clojure and Fennel this behaviour is always on.  
Works both when inserting and wrapping.  
The locations are not configurable ATM, but are grouped by language in the code - see, for example, the [Fennel config](parkour/cfg-fennel.lua#l24).

## `lispwords`_`.dialect.word`_

The plugin comes with a set of indentation rules for each supported file type,
but they are incomplete and sometimes will be wrong (due to multiple possible dialects under a single file type).  
The `lispwords` table allows customizing those rules by setting the desired number of distinguished arguments
a function/macro has. (`0` is like `defun` in Emacs.)  
As an example, see the built-in indent numbers [for Scheme](parkour/cfg-scheme.lua#l11).

## `repl_fifo`

This option can be set to the path of a named pipe from which a REPL (or anything, really) can read input.  
Inserting a newline at the end of the last line of a paragraph will send the paragraph to this pipe.  
Since REPL commands fit the criteria for paragraph (a top-level S-expression), they get sent as well.

## `emacs`

The plugin comes with two key themes - [emacs](keys-emacs.moon) and [CUA](keys-CUA.moon).  
`CUA` is always enabled.  
Setting this option to true will enable the `emacs` key theme, too. (A few CUA and built-in key bindings will be overridden.)

## `compat`

Setting this option to true will swap some of the motions in the native key theme with ones more resembling the built-in CUA motions.  
See [CUA-compat](keys-CUA-compat.moon).

__Note:__ The linked key themes are the only "documentation" on the available keyboard shortcuts ATM.

# Bugs

Howl has character-oriented buffer API, but Parkour works with byte offsets.  
In files with multibyte characters, word boundaries will not be where the plugin
expects them to be, and various issues will occur.  
There's some WIP in the `multibyte` branch on fixing this.
