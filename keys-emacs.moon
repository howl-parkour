H = ...

{
  editor:
    -- Structured motions:
    ctrl_alt_f: => @motion H.M.forward
    ctrl_alt_b: => @motion H.M.backward
    ctrl_alt_d: => @motion H.M.forward_down
    ctrl_alt_u: => @motion H.M.backward_up
    ctrl_alt_n: => @motion H.M.forward_up
    ctrl_alt_p: => @motion H.M.backward_down
    ctrl_alt_e: => @motion H.M.para_down
    ctrl_alt_a: => @motion H.M.para_up
    alt_e:      => @motion H.M.sentence_end
    alt_a:      => @motion H.M.sentence_start
    alt_b:      => @motion H.M.word_left_float
    alt_f:      => @motion H.M.word_right_end_float
    -- Structured selections:
    ctrl_alt_space: => @extend H.M.mark_sexp
    ctrl_alt_h:     => @extend H.T.mark_defun
    "ctrl_=":       => @extend H.T.expand_region
    -- Structured deletions:
    ctrl_alt_k:    => @operator H.O.change, H.M.forward, true
    ctrl_w:        => @operator H.O.change, nil, true
    alt_k:         => @operator H.O.change, H.M.sentence_end, true
    alt_d:         => @operator H.O.change, H.M.forward_word, true
    alt_backspace: => @operator H.O.change, H.M.backward_word, true
    ctrl_k:        => @operator H.O.change, H.M.line_end, true
    alt_0:
      ctrl_k:      => @operator H.O.change, H.M.home, true  -- FIXME: why isn't this triggered?
    -- Structured operators:
    alt_S:      => @operator H.O.split_sexp
    alt_J:      => @operator H.O.join_sexps
    "ctrl_}":   => @operator H.O.forward_barf
    "ctrl_)":   => @operator H.O.forward_slurp
    "ctrl_{":   => @operator H.O.backward_barf
    "ctrl_(":   => @operator H.O.backward_slurp
    alt_r:      => @operator H.O.raise_sexp
    "alt_?":    => @operator H.O.convolute_sexp
    alt_s:      => @operator H.O.splice_sexp
    "alt_(":    => @operator H.O.wrap_round, H.M.forward
    "alt_[":    => @operator H.O.wrap_square, H.M.forward
    "alt_{":    => @operator H.O.wrap_curly, H.M.forward
    'alt_"':    => @operator H.O.meta_doublequote, H.M.forward
    "alt_;":    => @operator H.O.wrap_comment, H.M.forward
    ctrl_alt_t: => @operator H.O.transpose_sexps
    alt_t:      => @operator H.O.transpose_words
    alt_down:   => @operator H.O.delete, H.M.forward_up, true
    alt_up:     => @operator H.O.delete, H.M.backward_up, true
    -- General operators:
    ctrl_d:        => @operator H.O.change, H.M.right
    alt_w:         => @operator H.O.yank, nil, true
    ctrl_y:        => @operator H.O.paste_reindent
    "ctrl_/":      'editor-undo'
    ctrl_g:
      "ctrl_/":    'editor-redo'
    alt_q:         => @operator H.O.format, H.T.mark_defun
    alt_enter:     => @operator H.O.open_next_line
    ctrl_f:        'cursor-right'
    ctrl_b:        'cursor-left'
    ctrl_n:        'cursor-down'
    ctrl_p:        'cursor-up'
    ctrl_e:        'cursor-line-end'
    ctrl_a:        'cursor-home'
    "alt_>":       'cursor-eof'
    "alt_<":       'cursor-start'
    alt_m:         'cursor-home-indent'
    "alt_)":       => @operator H.O.close_and_newline
    "alt_]":       => @operator H.O.close_and_newline
    "alt_}":       => @operator H.O.close_and_newline
    ctrl_alt_x:    => @operator H.O.eval_defun, H.T.paragraph
    ctrl_x:
      backspace:   => @operator H.O.change, H.M.sentence_start, true
      ctrl_x:      => if not @selection.empty then @selection\set @selection.cursor, @selection.anchor
      ctrl_s:      'save'
      d:           'project-open'
      b:           'switch-buffer'
      h:           'editor-select-all'
      k:           'buffer-close'
    ctrl_s:        'buffer-search-forward'
  binding_for:
    ["editor-delete-forward"]: => @operator H.O.change, H.M.right
    ["editor-delete-back"]:    => @operator H.O.change, H.M.left
}
