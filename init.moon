-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

-- would be nice if Howl had this prepared for plugins to use:
for dir in *howl.bundle.dirs
  package.path = tostring(dir).."/?.lua;" .. tostring(dir).."/?/init.lua;" .. package.path

-- XXX: just in case it gets fixed:
cwd = ...

-- XXX: just in case Howl switches to Lua > 5.1
unpack = table.unpack or unpack

init, supported = unpack require (cwd or "howl-parkour")..".parkour"

filetype =
  scm:  "scheme",  sch: "scheme",   ss: "scheme", sls: "scheme", sld: "scheme",
  lisp: "lisp",    lsp: "lisp",     el: "lisp",   cl: "lisp",
  clj:  "clojure", cljs: "clojure",
  fnl:  "fennel"

H = M: {}, T: {}, O: {}  -- handler functions
C = howl.config

class Keymaps
  tbl: {}
  new: =>
    @tbl = {}
  install: =>
    @tbl.CUA = (bundle_load "keys-CUA", H)
    howl.bindings.push @tbl.CUA
    if C.compat
      @tbl.compat = (bundle_load "keys-CUA-compat", H)
      howl.bindings.push @tbl.compat
    if C.emacs
      @tbl.emacs = (bundle_load "keys-emacs", H)
      howl.bindings.push @tbl.emacs
  remove: =>
    for _, keymap in pairs @tbl
      howl.bindings.remove keymap

keymaps = Keymaps!

with C
  .define name: "auto_square_brackets", type_of: "boolean", scope: "global", description: "Force square brackets where appropriate"
  .define name: "repl_fifo",            type_of: "string",  scope: "global", description: "Named fifo to send sexps to (REPL)"
  .define name: "emacs",                type_of: "boolean", scope: "global", description: "Enable emacs key bindings"
  .define name: "compat",               type_of: "boolean", scope: "global", description: "Enable more compatible key bindings"
  .define name: "lispwords",                                scope: "global", description: "Indent numbers for functions/macros"

env = {}
do_copy = false

howl.signal.connect "app-ready", ->
  C.watch("emacs", -> keymaps\remove!  keymaps\install!)
  C.watch("compat", -> keymaps\remove!  keymaps\install!)

C.lispwords = setmetatable {}, __index: (t, ftype) ->
  if supported[ftype]
    recfg = __newindex: (_, k, v) ->
      howl.signal.connect "file-opened", (args) ->
        {:file, :buffer} = args
        dialect = filetype[file.extension]
        if dialect == ftype
          env[buffer].lispwords[k] = v
    t[ftype] = setmetatable {}, recfg
    t[ftype]

howl.signal.connect "file-opened", (args) ->
  {:file, :buffer} = args
  dialect = filetype[file.extension]
  return if not supported[dialect]
  write = (pos, txt) ->
    buffer\insert txt, pos + 1
  delete = (pos, len) ->
    buffer\delete pos + 1, pos + len
  read = (base, len) ->
    return if base == buffer.size - 1
    base = base or 0
    len = len or buffer.size
    more = base + len < buffer.size
    (buffer\sub base + 1, base + len), more
  eol_at = (pos) ->
    (buffer.lines\at_pos pos + 1).end_pos - 1
  bol_at = (pos) ->
    (buffer.lines\at_pos pos + 1).start_pos - 1
  env[buffer] = init dialect, read, write, delete, eol_at, bol_at
  consumed_char = {"\n", " "}
  for ch in pairs env[buffer].parser.opposite do (table.insert consumed_char, ch) if #ch == 1
  env[buffer].consumed_char = "[" .. (table.concat consumed_char, "%") .. "]"

rewind = (args) ->
  {:buffer, :at_pos} = args
  return if not (buffer.file and supported[filetype[buffer.file.extension]] and
                 env[buffer] and env[buffer].parser.tree.is_parsed at_pos - 1)
  env[buffer].parser.tree.rewind at_pos - 1

howl.signal.connect "text-deleted", rewind
howl.signal.connect "text-changed", rewind
howl.signal.connect "text-inserted", rewind

seek = (cursor) ->
  (offset) ->
    cursor\move_to pos: offset + 1

howl.signal.connect "key-press", (args) ->
  {:parameters, :event, :source} = args
  editor = source == "editor" and parameters[1]
  buffer = editor and editor.buffer
  return if not (editor and buffer.file and
                 supported[filetype[buffer.file.extension]] and
                 event.character and
                 not (event.control or event.meta or event.alt or
                      event.key_name == "backspace" or
                      event.key_name == "delete"))
  input = env[buffer].input
  pos = editor.cursor.pos - 1
  range = start: pos, finish: pos
  if not editor.selection.empty
    start, finish = editor.selection\range!
    range = start: start - 1, finish: finish - 1
  char = event.character == "\r" and "\n" or event.character
  if char == "\n"
    sexp, parent = env[buffer].walker.sexp_at range, true
    if parent.is_root and (not sexp or (not sexp.is_comment and sexp.finish + 1 == range.start))
      same_line = not not sexp
      if not sexp
        line = buffer.lines\at_pos pos + 1
        prev_finish = env[buffer].walker.finish_before range
        if prev_finish
          (seek editor.cursor) prev_finish
          same_line = line == buffer.lines\at_pos editor.cursor.pos
          (seek editor.cursor) pos
      if sexp or same_line
        rl = env[buffer].walker.repl_line_at range
        rl.finish += 1
        H.O.eval_defun env[buffer], rl, pos
  M = buffer.config
  consumed_char = char\find env[buffer].consumed_char
  flag = {}
  if consumed_char
    buffer\as_one_undo ->
      flag.set = input.insert range, (seek editor.cursor), char, M.auto_square_brackets
  else
    flag.set = input.insert range, (seek editor.cursor), char, M.auto_square_brackets
  howl.signal.abort if flag.set

howl.signal.connect "after-buffer-switch", (args) ->
  {:current_buffer, :editor} = args
  keymaps\remove!
  return if not current_buffer.file
  dialect = filetype[current_buffer.file.extension]
  return if not supported[dialect]
  keymaps\install!
  editor.mode_at_cursor.auto_pairs = {}
  editor.motion = (func) =>
    pos = @cursor.pos - 1
    range = start: pos, finish: pos
    newpos = func env[current_buffer], range
    if newpos and newpos ~= pos then (seek @cursor) newpos
  editor.extend = (func) =>
    pos = @cursor.pos - 1
    range = start: pos, finish: pos
    local backward
    if not @selection.empty
      start, finish = @selection\range!
      range = start: start - 1, finish: finish - 1
      backward = @selection.cursor < @selection.anchor
    newpos, textobject_finish = func env[current_buffer], range, pos
    local left, right
    if newpos and not textobject_finish
      left, right = (math.min range.start, range.finish, newpos) + 1, (math.max range.start, range.finish, newpos) + 1
    elseif newpos and textobject_finish
      left, right = (math.min newpos, textobject_finish) + 1, (math.max newpos, textobject_finish) + 2
    if left and right
      @selection\set (backward and right or left), (backward and left or right)
  editor.operator = (func, motion, _copy) =>
    pos = @cursor.pos - 1
    range = start: pos, finish: pos
    if not @selection.empty
      start, finish = @selection\range!
      range = start: start - 1, finish: finish - 1
    elseif motion
      start, finish = motion env[current_buffer], range
      if start and finish
        range = start: start, finish: finish + 1
      elseif start
        range = start: (math.min pos, start), finish: (math.max pos, start)
    t = {}
    current_buffer\as_one_undo ->
      if _copy
        do_copy = true
      t.newpos, fragments = func env[current_buffer], range, pos
      do_copy = false
      if _copy and fragments and #fragments > 0
        clip = table.concat fragments
        howl.clipboard.push clip
    if t.newpos then (seek @cursor) t.newpos

-- motions:

is_comment = (t) -> t.is_comment
startof = (t) -> t.start

H.M.word_right_end  = (range) => @walker.finish_after range, is_comment
H.M.word_right_end_float = (range) => (@walker.finish_float_after range)
H.M.word_left       = (range) => @walker.start_before range, is_comment
H.M.word_left_float = (range) => (@walker.start_float_before range)
H.M.forward_down    = (range) => @walker.start_down_after range, is_comment
H.M.backward_up     = (range) => @walker.start_up_before range
H.M.forward_up      = (range) => @walker.finish_up_after range
H.M.backward_down   = (range) => @walker.finish_down_before range, is_comment
H.M.para_up         = (range) =>
  sexp = @parser.tree.before range.start, startof, is_comment
  sexp and sexp.start
H.M.para_down       = (range) =>
  parent = @walker.paragraph_at range
  sexp = @parser.tree.after parent and parent.finish or range.finish, startof, is_comment
  sexp and sexp.start
H.M.right           = (range) => range.finish + 1
H.M.left            = (range) => range.start - (range.start > 0 and 1 or 0)
H.M.line_end        = (range) => (@walker.next_finish_wrapped range)
H.M.home            = (range) => @walker.prev_start_wrapped range
H.M.sentence_end    = (range) => @walker.anylist_finish range
H.M.sentence_start  = (range) => @walker.anylist_start range

H.M.next_section    = (range) =>
  newpos = @walker.find_after range, (t) -> t.section and t.start > range.start
  newpos and newpos.start or howl.app.editor.buffer.size

H.M.prev_section    = (range) =>
  newpos = @walker.find_before range, (t) -> t.section
  newpos and newpos.start or 0

H.M.mark_sexp       = (range, pos) =>
  if pos < range.finish
    H.M.word_left self, range
  else
    H.M.word_right_end self, range

H.M.backward        = (range) => @walker.start_before range, is_comment
H.M.forward         = (range) => @walker.finish_after range, is_comment
-- This motion is only for use with delete
H.M.backward_word   = (range) => (@walker.start_float_before range, true)
-- This motion is only for use with delete
H.M.forward_word    = (range) => (@walker.finish_float_after range, true)

-- textobjects:

H.T.paragraph = (range) =>
  sexps = @walker.repl_line_at range
  if sexps then sexps.start, sexps.finish

H.T.mark_defun = (range) =>
  sexp = @walker.paragraph_at range
  if sexp and not sexp.is_line_comment then sexp.start, sexp.finish

H.T.expand_region = (range) =>
  start, finish = @walker.wider_than range
  start, finish and finish - 1

H.T.line = (range) =>
  l = howl.app.editor.buffer.lines\at_pos range.start + 1
  l.start_pos - 1, l.end_pos - 1

-- operators:

make_yank = (handler, fragments) ->
  (range) ->
    txt = howl.app.editor.buffer\sub range.start + 1, range.finish
    if txt
      table.insert fragments, 1, txt
    handler and handler range

_delete = (range) ->
  howl.app.editor.buffer\delete range.start + 1, range.finish

H.O.delete = (range, pos) =>
  fragments = {}
  delete_maybe_yank = do_copy and make_yank(_delete, fragments) or _delete
  splicing = true
  newpos = @edit.delete_splicing range, pos, splicing, delete_maybe_yank
  newpos, fragments

H.O.change = (range, pos) =>
  fragments = {}
  delete_maybe_yank = do_copy and make_yank(_delete, fragments) or _delete
  newpos = @edit.delete_nonsplicing range, pos, delete_maybe_yank
  newpos, fragments

H.O.yank = (range, pos) =>
  fragments = {}
  yank = make_yank nil, fragments
  action = kill:false, wrap: true, splice: false, func: yank
  @edit.pick_out range, pos, action
  range.start, fragments

H.O.paste_reindent = =>
  editor = howl.app.editor
  buffer = editor.buffer
  if howl.clipboard.current
    buffer\as_one_undo ->
      editor\paste!
      pos = editor.cursor.pos - 1
      cursor = start: pos, finish: pos
      _, parent = @walker.sexp_at cursor, true
      @edit.refmt_at parent, cursor

H.O.format = (range, pos) =>
  @edit.refmt_at range, {start: pos, finish: pos}

local repl_fifo

H.O.eval_defun = (range, pos) =>
  M = howl.app.editor.buffer.config
  if not M.repl_fifo or range.finish == range.start then return pos
  if pos > range.finish then return pos
  unbalanced = @parser.tree.unbalanced_delimiters range
  if unbalanced and #unbalanced > 0 then return pos
  local errmsg
  if not repl_fifo
    repl_fifo, errmsg = io.open M.repl_fifo, "a+"
  if repl_fifo
    repl_fifo\write (howl.app.editor.buffer\sub range.start + 1, range.finish), "\n"
    repl_fifo\flush!
  elseif errmsg
    log.warn errmsg
  pos

-- flush any code stuck in the fifo, so starting a REPL after the file has been closed will not read old stuff in.
howl.signal.connect "buffer-closed", (args) ->
  {:buffer} = args
  dialect = buffer.file and filetype[buffer.file.extension]
  return if not supported[dialect]
  env[buffer] = nil
  if repl_fifo
    for _ in pairs env  -- instead of "quit" event
      return
    M = buffer.config
    repl_fifo\close!
    repl_fifo = io.open M.repl_fifo, "w+"
    if repl_fifo
      repl_fifo\close!
      repl_fifo = nil

H.O.block_comment = (range, pos) =>
  sexp = @walker.sexp_at range, true
  if sexp and not sexp.is_comment
    @edit.wrap_comment {start: sexp.start, finish: sexp.finish + 1}, pos
  elseif sexp
    @edit.splice_anylist range, pos

H.O.wrap_comment = (range, pos) =>
  @edit.wrap_comment {start: pos, finish: range.finish + 1}, pos

H.O.close_and_newline = (range) => @edit.close_and_newline range
H.O.open_next_line    = (range) => @edit.close_and_newline range, "\n"

H.O.raise_sexp       = (range, pos) => @edit.raise_sexp range, pos
H.O.splice_sexp      = (range, pos) => @edit.splice_anylist range, pos
H.O.split_sexp       = (range)      => @edit.split_anylist range
H.O.join_sexps       = (range)      => @edit.join_anylists range
H.O.convolute_sexp   = (range)      => @edit.convolute_lists range, pos
H.O.transpose_sexps  = (range)      => @edit.transpose_sexps range
H.O.transpose_words  = (range)      => @edit.transpose_words range
H.O.wrap_round       = (range, pos) => @edit.wrap_round range, pos, C.auto_square_brackets
H.O.wrap_square      = (range, pos) => @edit.wrap_square range, pos, C.auto_square_brackets
H.O.wrap_curly       = (range, pos) => @edit.wrap_curly range, pos, C.auto_square_brackets
H.O.meta_doublequote = (range, pos) => @edit.meta_doublequote range, pos, C.auto_square_brackets
H.O.forward_barf     = (range)      => @edit.barf_sexp range, true
H.O.backward_barf    = (range)      => @edit.barf_sexp range
H.O.forward_slurp    = (range)      => @edit.slurp_sexp range, true
H.O.backward_slurp   = (range)      => @edit.slurp_sexp range

howl.signal.connect "editor-focused", (args) ->
  {:editor} = args
  keymaps\remove!
  return if not editor.buffer.file
  dialect = filetype[editor.buffer.file.extension]
  if supported[dialect]
    keymaps\install!

unload = ->
  env = {}
  keymaps\remove!

{
  info:
    author: "Georgi Kirilov"
    description: "Structured editing of S-expressions"
    license: "TBD"
  :unload
}
