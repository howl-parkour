H = ...

{
  editor:
    alt_right:      => @motion H.M.forward_down
    alt_left:       => @motion H.M.backward_up
    alt_up:         => @motion H.M.forward_up
    alt_down:       => @motion H.M.backward_down
    ctrl_down:      => @motion H.M.para_down
    ctrl_up:        => @motion H.M.para_up
    ctrl_page_down: => @motion H.M.next_section
    ctrl_page_up:   => @motion H.M.prev_section
    ctrl_shift_up:   => @extend H.T.mark_defun
    ctrl_shift_down: => @extend H.T.mark_defun
    ctrl_M:          => @extend H.T.expand_region
    "ctrl_alt_/":   => @operator H.O.split_sexp
    ctrl_J:         => @operator H.O.join_sexps
    "ctrl_alt_.":   => @operator H.O.forward_slurp
    "ctrl_alt_>":   => @operator H.O.backward_barf
    "ctrl_alt_,":   => @operator H.O.forward_barf
    "ctrl_alt_<":   => @operator H.O.backward_slurp
    ctrl_alt_i:     => @operator H.O.raise_sexp
    "alt_?":        => @operator H.O.convolute_sexp
    "ctrl_alt_\\":  => @operator H.O.splice_sexp
    "alt_(":        => @operator H.O.wrap_round, H.T.expand_region
    "alt_[":        => @operator H.O.wrap_square, H.T.expand_region
    "alt_{":        => @operator H.O.wrap_curly, H.T.expand_region
    'alt_"':        => @operator H.O.meta_doublequote, H.T.expand_region
    ctrl_alt_up:    => @operator H.O.delete, H.M.forward_up  -- splice_sexp_killing_forward
    ctrl_alt_left:  => @operator H.O.delete, H.M.backward_up  -- splice_sexp_killing_backward
    ctrl_alt_x:     => @operator H.O.eval_defun, H.T.paragraph
  binding_for:
    ["cursor-word-right-end"]: => @motion H.M.word_right_end
    ["cursor-word-left"]:      => @motion H.M.word_left
    ["editor-delete-forward"]:        => @operator H.O.change, H.M.right
    ["editor-delete-back"]:           => @operator H.O.change, H.M.left
    ["editor-delete-forward-word"]:   => @operator H.O.change, H.M.word_right_end
    ["editor-delete-back-word"]:      => @operator H.O.change, H.M.word_left
    ["editor-delete-to-end-of-line"]: => @operator H.O.change, H.M.line_end, true
    ["editor-delete-line"]:           => @operator H.O.change, H.T.line
    ["editor-reflow-paragraph"]:      => @operator H.O.format, H.T.mark_defun
    ["editor-indent-all"]:            => @operator H.O.format, H.T.mark_defun  -- TODO: refmt_at does not work at top-level yet
    ["editor-indent"]:                => @operator H.O.format, H.T.mark_defun
    ["editor-smart-tab"]:             => @operator H.O.format, H.T.mark_defun
    ["editor-smart-back-tab"]:        => @operator H.O.format, H.T.mark_defun
    ["editor-toggle-comment"]:        => @operator H.O.block_comment
    ["editor-newline-below"]:         => @operator H.O.open_next_line
    ["editor-cut"]:                   => @operator H.O.change, nil, true
    ["editor-copy"]:                  => @operator H.O.yank, nil, true
    ["editor-paste"]:                 => @operator H.O.paste_reindent
    ["cursor-word-right-end-extend"]: => @extend H.M.word_right_end
    ["cursor-word-left-extend"]:      => @extend H.M.word_left
    ["cursor-line-end-extend"]:       => @extend H.M.line_end
    ["cursor-home-extend"]:           => @extend H.M.home
}
