H = ...

{
  binding_for:
    ["cursor-word-right-end"]: => @motion H.M.word_right_end_float
    ["cursor-word-left"]:      => @motion H.M.word_left_float
    ["editor-delete-forward-word"]:   => @operator H.O.change, H.M.word_right_end_float
    ["editor-delete-back-word"]:      => @operator H.O.change, H.M.word_left_float
    ["cursor-word-right-end-extend"]: => @extend H.M.word_right_end_float
    ["cursor-word-left-extend"]:      => @extend H.M.word_left_float
}
